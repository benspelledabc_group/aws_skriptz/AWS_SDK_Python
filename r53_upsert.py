import boto3
import subprocess


def create_update_entry(update_type, pointer, pointer_value, record_type):
    client = boto3.client('route53')
    response = client.change_resource_record_sets(
        ChangeBatch={
            'Changes': [
                {
                    'Action': update_type,
                    'ResourceRecordSet': {
                        'Name': pointer,
                        'ResourceRecords': [
                            {
                                'Value': pointer_value,
                            },
                        ],
                        'TTL': 60,
                        'Type': record_type,
                    },
                },
            ],
            'Comment': 'Web Server',
        },
        HostedZoneId='Z05656112YHDO45U4XQ99',
)

def discover_public_ip():
    #bashCommand = "cwm --rdf test.rdf --ntriples > test.nt"
    bashCommand = "/usr/bin/getPublicIP"
    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()

def show_ip():
    rval = ''
    with open('/opt/public-ip/info', 'r') as reader:
        line = reader.readline()
        while line != '':
            rval = line
            #print(line, end='')
            line = reader.readline()
    return rval


# ########### DISCOVERY PHASE ############
discover_public_ip()
public_ip = show_ip().strip()



# Show your work
print(public_ip)
create_update_entry('UPSERT', 'test.spelledabc.org', public_ip, 'A')
create_update_entry('UPSERT', 'spelledabc.org', public_ip, 'A')

