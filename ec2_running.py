import json
from pprint import pprint

# put values in ~/.aws/credentials
#[default]
#aws_access_key_id = AK--REDACTED--PLV3
#aws_secret_access_key = TRd--REDACTED--7E

class ResultSet:
    availability_zone = ""
    id = 0
    name = ""
    type = ""
    key = ""
    private_ip = ""
    public_ip = ""


def do_work():
  import csv
  import boto3

  ec2=boto3.client(service_name="ec2")

  all_regions=[]
  for each_region in ec2.describe_regions()['Regions']:
      all_regions.append(each_region['RegionName'])

  fo=open("aws_instances.csv", 'w', newline='')
  data_obj=csv.writer(fo)
  data_obj.writerow(["AvailibilityZone", "InstanceID", "InstanceName", "InstanceType", "KeyName", "Private IP", "Public IP"])

  for each_region in all_regions:
      instance_name=""
      ec2_re=boto3.resource(service_name='ec2', region_name=each_region)

      #for iir in ec2_re.instances.all():
      #filter only active/running instances.
      for iir in ec2_re.instances.filter(Filters=[{'Name': 'instance-state-name', 'Values': ['running']}]):
          # get instance name if set, - if not.
          for tags in iir.tags:
              if tags["Key"] == 'Name':
                  instance_name = tags["Value"]
          if len(instance_name) < 1:
              instance_name = "NameNotSet"

          for iface in iir.network_interfaces:
              azone=iface.subnet.availability_zone

          data_obj.writerow([azone, iir.instance_id, instance_name, iir.instance_type, iir.key_name, iir.private_ip_address, iir.public_ip_address])

          # its better in json...
          #print(azone, iir.instance_id, instance_name, iir.instance_type, iir.key_name, iir.private_ip_address, iir.public_ip_address)
          x = ResultSet()
          x.availability_zone = azone
          x.id = iir.instance_id
          x.name = instance_name
          x.type = iir.instance_type
          x.key = iir.key_name
          x.private_ip = iir.private_ip_address
          x.public_ip = iir.public_ip_address

          #convert to JSON string
          json_output = json.dumps(x.__dict__)
          print(json_output)

  fo.close()


do_work()

